(function main( $ ) {
  'use strict';

  var topics = $.getJSON( 'getTopics.php', function() {
    visualizeTopics( topics.responseJSON );
  } );

  var visualizeTopics = function visualizeTopics( topics ) {
    var tblHTML = '';
    var rows = [
      'Description',
      'Epics',
      'State',
      'Objectives',
      'Stories',
      'KPIs',
      'Stakeholders',
      'PortfolioTickets'
    ];
    var numTopics = topics.length;

    $.each( rows, function iterateRows( key, prop ) {
      tblHTML += '<tr><th colspan="' + numTopics + '">' + prop + '</th></tr><tr>';

      $.each( topics, function iterateTopics( key, topic ) {
        var element = topic[ prop ];

        if ( $.isArray( element ) ) {
          tblHTML += '<td><ul>';
          $.each( element, function iterateArray( key, entry ) {
            tblHTML += '<li>' + visualizeEntry( entry ) + '</li>';
          } );
          tblHTML += '</ul></td>';
        } else {
          tblHTML += '<td>' + ( prop === 'Stories' ? visualizeStories( topic.ID ) : element ) + '</td>';
        }
      } );

      tblHTML += '</tr>';
    } );

    $( '#mainTbl' ).html( tblHTML );
  };

  var visualizeEntry = function visualizeEntry( entry ) {
    if ( typeof entry === 'object' ) {
      return '<img src="' + entry.state + '.png"/> ' + entry.description;
    } else {
      return entry;
    }
  };

  var visualizeStories = function visualizeStories( topic ) {
    var html = '<div id="stories_' + topic + '"><div class="spinner"></div></div>';

    addStoriesToBoard( topic );

    return html;
  };

  var addStoriesToBoard = function addStoriesToBoard( topic ) {
    var stories = $.getJSON( 'getIssues.php?topic=' + topic, function() {
      var trgtEl = $( '#stories_' + topic );
      var storyData = stories.responseJSON;
      var html = '';

      $.each( storyData.issues , function(i, val) {
          var storyHtml = '<div id="story_' + val.id + '"><ul>';
          storyHtml += '<li> <a href="https://jira.hrs.io/browse/' + val.key + '" target="_blank">' + val.key + '</a>:' + val.fields.summary + '</li>';
          storyHtml += '</ul></div>';

            html += storyHtml;
      });

      trgtEl.html( html );
    } );
  };

}( $ ));
