<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$curl = curl_init();
$api_credentials = base64_encode("api:silly_passw0rd");

$topic = preg_replace( "/(\w+).*/", "$1", $_REQUEST['topic'] );
$url = "https://jira.hrs.io/rest/api/2/search/?jql=project%3DCM+and+topic~".urlencode($topic)."&fields=customfield_12800,fixVersions,priority,labels,versions,issuetype,description,summary,environment";
$auth_header = "Authorization: Basic ".$api_credentials;

curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    $auth_header,
    "Cache-Control: no-cache",
    "Content-Type: application/json"
    ),
));

$issuesResponse = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  print($issuesResponse);
}
?>